#!/bin/bash

for file in $1/*.cpp; do
	echo $file
	python3 ./encore_to_edge.py -f $file
done