import re
import argparse
from shutil import move, copymode
from os import fdopen, remove
from tempfile import mkstemp

func = lambda s: s[:1].lower() + s[1:] if s else ''
def encore_to_edge(filename):
	#regex1 = re.compile("pdrv->(get|set)_([a-zA-Z0-9]*)_?([a-zA-Z0-9]*)?_?([a-zA-Z0-9]*)?")
	regex1 = re.compile("(.*)->(get|set)_([a-zA-Z0-9]*)_?([a-zA-Z0-9]*)?_?([a-zA-Z0-9]*)?")
	
	fh, abs_path = mkstemp()
	new_file= fdopen(fh,'w')
	with open(filename) as f:
	    for line in f:
	        result = regex1.search(line)
	        if result:
	        	temp=result.groups()[0]+"->"
	        	for element in result.groups()[2:]:
	        		if element:
	        			temp+=func(element)+"."
	        	temp+=result.groups()[1]
	        	new_line=line[0:result.start()]+temp+line[result.end():-1]
	        	new_file.write(new_line+"\n")
	        	continue
	        new_file.write(line)
	new_file.close()
	copymode(filename, abs_path)
	move(filename, filename+".bak") 
	move(abs_path, filename)       

if __name__ == "__main__":
  parser=argparse.ArgumentParser()
  parser.add_argument("-filename","-f",required=True)
  args = parser.parse_args()
  encore_to_edge(args.filename)
