# encore_to_edge

Collection of scripts to convert encore fesa classes to edge classes

encore_to_edge.py:  

converts pdrv->get_rfFrontend_control to pdrv->rfFrontend.control.get  
pdrv->set_rfFrontend_control to pdrv->rfFrontend.control.set  
works with any number of submap depth  
creates a backup file of the old file called filename.cpp.bak  
  
encore_to_edge.sh:  
runs encore_to_edge.py for every file with .cpp extension in the folder passed as first argument  

edge_static_cast.py:  
adds static_cast to the appropriate type for every set. The type is found in the memory_nodes.hpp from the device wrapper  
example:  
python edge_static_cast.py -f rfFrontEnd.cpp -header ../rf_lhc_beampos/driver_wrappers/rf-lhc_beampos-lib/src/rf-lhc_beampos-lib/memory_nodes.hpp  
creates a backup file of the old file called filename.cpp.bak  

edge_static_cast.sh  
runs edge_static_cast.py on every file with the .cpp extension in the folder passed as the first argument  
example: ./edge_static_cast.sh ./server ../rf_lhc_beampos/driver_wrappers/rf-lhc_beampos-lib/src/rf-lhc_beampos-lib/memory_nodes.hpp   
