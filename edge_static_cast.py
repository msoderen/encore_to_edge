import re
import argparse
from shutil import move, copymode
from os import fdopen, remove
from tempfile import mkstemp

func = lambda s: s[:1].lower() + s[1:] if s else ''

def findType(header,namespaces):
	header_file=open(header,'r')
	#print(namespaces)
	regex_strings=[]
	regexes=[]
	#find correct namespace
	for element in namespaces:
		regex_strings.append("namespace "+element+" {")
		regexes.append(re.compile(regex_strings[-1]))
	
	#find class definition
	regex_strings.append("class R"+namespaces[-1].capitalize()+";")
	regexes.append(re.compile(regex_strings[-1],re.IGNORECASE))
	
	#find typedef
	regex_strings.append("typedef.*\<.*\> Proxy;")
	regexes.append(re.compile(regex_strings[-1],re.IGNORECASE))
	
	#find type
	regex_strings.append("class r"+namespaces[-1]+": public.*\<(.*)\>")
	regexes.append(re.compile(regex_strings[-1],re.IGNORECASE))
	
	


	#print(regex_strings)
	counter=0
	for line in header_file:
		result = regexes[counter].search(line)
		if result and counter==(len(regexes)-1):
			header_file.close()
			return result.groups()[0]
			#print(result)
			#print(result.groups())
			#print("success")
		elif result:
			counter+=1
		else:
			counter=0

	#print(regex_strings)
	#regex1=re.compile(regex_str)
	#regex1 = re.compile("(.*)->([a-zA-Z0-9]*).?([a-zA-Z0-9]*)?.?([a-zA-Z0-9]*)?.set\((.*)\);")
	header_file.close()   
	pass


def encore_to_edge(filename,header):

	regex1 = re.compile("(.*)->([a-zA-Z0-9]*).?([a-zA-Z0-9]*)?.?([a-zA-Z0-9]*)?.set\((.*)\);")
	fh, abs_path = mkstemp()
	new_file= fdopen(fh,'w')
	with open(filename) as f:
	    for line in f:
	        result = regex1.search(line)
	        if result:
	        	if "static_cast" in result.groups()[-1]:
	        		new_file.write(line)
	        		continue
	        	datatype=findType(header,[ x for x in result.groups()[1:-1] if x])
	        	if datatype==None:
	        		#print("type not found")
	        		new_file.write(line)
	        		continue
	        	temp=result.groups()[0]+"->"
	        	for element in result.groups()[1:-1]:
	        		if element:
	        			temp+=func(element)+"."
	        	temp+="set(static_cast<"+datatype+">("+result.groups()[-1]+"));"
	        	new_line=line[0:result.start()]+temp+line[result.end():-1]
	        	#print(new_line)
	        	new_file.write(new_line+"\n")
	        	continue
	        new_file.write(line)
	new_file.close()
	copymode(filename, abs_path)
	move(filename, filename+".bak1") 
	move(abs_path, filename)
    

if __name__ == "__main__":
  parser=argparse.ArgumentParser()
  parser.add_argument("-filename","-f",required=True)
  parser.add_argument("-header",required=True)
  args = parser.parse_args()
  encore_to_edge(args.filename,args.header)
