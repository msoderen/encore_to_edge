#!/bin/bash

for file in $1/*.cpp; do
	echo $file
	python3 ./edge_static_cast.py -f $file -header $2
done